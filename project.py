"""
Course Project

cwischhoefer20@georgefox.edu
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import sklearn.svm
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, balanced_accuracy_score

import cartopy.crs
import cartopy.feature


# Load your data from file, handling any missing data or extreme outliers appropriately.
earthquake_data = pd.read_csv('earthquake_data.csv')

# Create a LabelEncoder object
le = LabelEncoder()

# Ordinal encode the values of alert
earthquake_data['alerts_encoded'] = le.fit_transform(earthquake_data['alert'])

# magnitude, cdi, mmi, alert (encoded), sig, nst, dmin, gap, depth
earthquake_features = earthquake_data[['magnitude','cdi', 'mmi', 'sig', 'nst', 'dmin', 'depth', 'alerts_encoded']]

model = sklearn.svm.SVC()
model.fit(earthquake_features, earthquake_data['tsunami'])
tsunami_predicted = model.predict(earthquake_features)
earthquake_data['tsunami_pred'] = tsunami_predicted.tolist()

# perform cross validation
raw_scores = cross_val_score(model, earthquake_features, earthquake_data['tsunami'], cv=10, scoring='accuracy')
bal_scores = cross_val_score(model, earthquake_features, earthquake_data['tsunami'], cv=10, scoring='balanced_accuracy')
print("Raw accuracy per fold:", raw_scores)
print("Balanced accuracy per fold:", bal_scores)


# Compute overall raw accuracy and balanced accuracy
overall_raw = accuracy_score(earthquake_data['tsunami'], model.predict(earthquake_features))
overall_bal = balanced_accuracy_score(earthquake_data['tsunami'], model.predict(earthquake_features))
print("Overall raw accuracy:", overall_raw)
print("Overall balanced accuracy:", overall_bal)

matrix = sklearn.metrics.confusion_matrix(earthquake_data['tsunami'], earthquake_data['tsunami_pred'], labels=None, sample_weight=None, normalize=None)
print(matrix)
sns.heatmap(matrix, annot=True, fmt='d')
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.show()

### PLOT 1
# Bar plot that shows percentage of earthquakes that resulted in a tsunami
alert_data = earthquake_data.groupby('alert')['tsunami'].mean() * 100

# Create a list of alert levels and a list of percentages
alerts = alert_data.index.tolist()
percentages = alert_data.tolist()

# Create a bar plot with colors
colors = ['green', 'yellow', 'orange', 'red']
plt.bar(alerts, percentages, color=colors)

# Add a title and labels
plt.title('Percentage of Tsunamis per Alert Level')
plt.xlabel('Alert Level')
plt.ylabel('Percentage of Tsunamis')

# Show the plot
plt.show()



### PLOT 2
# Show on the map where the model predicted correctly

colors = ['red' if (earthquake_data['tsunami'])[i] != (earthquake_data['tsunami_pred'])[i] else 'green' for i in range(len((earthquake_data['tsunami'])))]

plt.figure(figsize=(12,6))
plt.title("Tsunamis correctly predicted")
ax = plt.axes(projection=cartopy.crs.PlateCarree())
ax.stock_img()

# plot where tsunamis occured
earthquake_data_tsunami = earthquake_data.where(earthquake_data['tsunami_pred'] > 0)
plt.scatter(earthquake_data_tsunami['longitude'], earthquake_data_tsunami['latitude'], c=colors, s=earthquake_data['magnitude']*10, alpha=0.5, label='model predicted correctly')
plt.legend()
plt.show()

### PLOT 3
# Show the how frequent different magnitudes are for earthquakes
plt.hist(earthquake_data['magnitude'], bins=20, color='blue')

plt.ylabel('Frequency')
plt.xlabel('Magnitude')
plt.title('Distribution of Earthquake Magnitudes')

# Show plot
plt.show()
