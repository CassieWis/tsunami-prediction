# Project Title
Prediction of a Tsunami after an Earthquake

## Data

Dataset used: https://www.kaggle.com/datasets/warcoder/earthquake-dataset

This dataset contains detailed information about earthquakes around the world and contains a boolean value to inform if oceanic changes (aka, tsunami) occurred from the earthquake.

The columns include:

- **title**: title name given to the earthquake
magnitude: The magnitude of the earthquake
date_time: date and time
- **cdi**: The maximum reported intensity for the event range
- **mmi**: The maximum estimated instrumental intensity for the event
alert: The alert level - “green”, “yellow”, “orange”, and “red”
- **tsunami**: "1" for events in oceanic regions and "0" otherwise
- **sig**: A number describing how significant the event is. Larger numbers indicate a more significant event. This value is determined on a number of factors, including: magnitude, maximum MMI, felt reports, and estimated impact
- **net**: The ID of a data contributor. Identifies the network considered to be the preferred source of information for this event.
- **nst**: The total number of seismic stations used to determine earthquake location.
- **dmin**: Horizontal distance from the epicenter to the nearest station
- **gap**: The largest azimuthal gap between azimuthally adjacent stations (in degrees). In general, the smaller this number, the more reliable is the calculated horizontal position of the earthquake. Earthquake locations in which the azimuthal gap exceeds 180 degrees typically have large location and depth uncertainties
- **magType**: The method or algorithm used to calculate the preferred magnitude for the event
- **depth**: The depth where the earthquake begins to rupture
- **latitude / longitude**: coordinate system by means of which the position or location of any place on Earth's surface can be determined and described
- **location**: location within the country
- **continent**: continent of the earthquake hit country
- **country**: affected country
## Experimental Design

I will be using the columns magnitude, cdi, mmi, sig, and depth to train the model. These are integer values that tell of different aspects of the earthquake that overall lead to the calculated destruction.

The predicted column is Tsunami.

## Discussion

The dataset will need to be cleaned, as there are both missing and extra commas, along with many columns not needed for the experiment.

[See the Final Presentation Slides](slides.pdf)
